#!/bin/bash

######################################################
###                    VARIABLES
######################################################

API_LINK="https://gitlab.com/api/v4/"
COMMIT_MSG_PREFIX="Update modules"


######################################################
###                    FUNCTIONS
######################################################

UPDATES=""
update () {
    temp=`/app/flatpak-external-data-checker --verbose --edit-only "$1" \
        | grep "CHANGE SOON:\|BROKEN:" \
        | cut -d ":" -f 2`
    if [[ ! -z "$temp" ]]; then
    UPDATES+=`echo $temp \
            | sed "s/ /\\\n/g"`
    UPDATES+="\n"
    fi
}

update_loop () {
    for f in *.json; do
        update "$f"
    done
}


push_updates () {
    COMMIT_DESC="$UPDATES"
    UPDATES=$(printf "%s " `tr ' ' '\n' <<< "$UPDATES" | sort -u -f`)
    if [[ `git status --porcelain` ]]; then
        HASH=$(echo -e "$UPDATES" | sha256sum | cut -c1-8 )
        if (curl "${API_LINK}merge_requests?state=opened&search=$HASH&private_token=$TOKEN" | grep -q "$COMMIT_MSG_PREFIX $HASH") ; then
            echo "Found existing pull request"
        else
            LAST_COMMIT=`git log --oneline -n1 | cut -d ' ' -f1-1`
            git checkout -b update-$LAST_COMMIT-$HASH
            git add -u
            echo -e "$COMMIT_MSG_PREFIX $HASH\n\n$COMMIT_DESC" | git commit --file -
            MR_DESC=`printf "%s" "$UPDATES" | sed "s/ /<br>/g"`
            MR_DESC=`printf "%s" "$MR_DESC" | sed "s/\\\\\n/<br>/g"`
            printf "%s" "$MR_DESC"
            git push http://root:$TOKEN@$CI_SERVER_HOST/$CI_PROJECT_PATH.git \
                HEAD:update-$LAST_COMMIT-$HASH \
                -o merge_request.create \
                -o merge_request.title="$COMMIT_MSG_PREFIX $HASH" \
                -o merge_request.description="$MR_DESC"
        fi
    else
        echo "No change"
    fi
}


######################################################
###                    ENTRYPOINT
######################################################

if [ $# -eq 0 ]; then
    update_loop
    push_updates
elif [ "$1" -eq "file" ]; then
    update "$1"
fi
